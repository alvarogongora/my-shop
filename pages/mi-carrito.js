import { LitElement, html, css } from 'lit';

export class MiCarrito extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
                padding: 20px;
            }

            .cart-container {
                background-color: #f5f5f5;
                border: 1px solid #ddd;
                padding: 20px;
                border-radius: 5px;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            }

            .cart-header {
                font-size: 24px;
                margin-bottom: 20px;
            }

            .cart-items {
                list-style: none;
                padding: 0;
            }

            .cart-item {
                display: flex;
                justify-content: space-between;
                align-items: center;
                border-bottom: 1px solid #ddd;
                padding: 10px 0;
            }

            .item-name {
                flex-grow: 1;
            }

            .item-price {
                margin-left: 20px;
                font-weight: bold;
                color: #ab0e89;
            }

            .checkout-button {
                margin-top: 20px;
                background-color: #333;
                color: white;
                padding: 10px 20px;
                border: none;
                border-radius: 5px;
                cursor: pointer;
            }
        `
    ];

    static get properties() {
      return {
        cart: { type: Array },
        idProduct: {type: Number}
      };
    }

    render() {
        return html`
            
                ${this.cart.map(sneaker => 
                html`
                <div class="cart-container">
                <h1 class="cart-header">CARRITO</h1>
                
                <ul class="cart-items">
                    <li class="cart-item">
                        <span class="item-name">${sneaker.name}</span>
                        <span class="item-size">Talla: ${sneaker.size}</span>
                        <span class="item-price">${sneaker.price}€</span>
                        
                        <button  @click =${(e) => this.deleteProduct(e,sneaker.id)}>Eliminar</button>
                        
                    </li>
                </ul>
                <button class="checkout-button">Realizar Pedido</button>
                </div>
                `
                
            )}
         
            
        `;
    }

    deleteProduct(e,id){
        this.dispatchEvent(
            new CustomEvent("delete-product",{
                detail: id,
                bubbles:true,
                composed:true
            })

        )
    }
    
}

customElements.define('mi-carrito', MiCarrito);
