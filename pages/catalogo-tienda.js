import { LitElement, html, css } from 'lit-element';


export class CatalogoTienda extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
            }
            .section{
                display: grid;
                grid-template-columns: 1fr 4fr;
                font-family:verdana;
            }
            .filter{
                font-size:20px;
                font-family: Verdana, Geneva, Tahoma, sans-serif;
               
            }
            .categorias{
                border-right:1px solid black;
                padding:left;
            }
            .size38,
            .size39,
            .size40,
            .size41,
            .size42,
            .size43,
            .size44,
            .size45,
            .size46{
                padding:10px;
                border-style:solid;
                display:inline-block;
                border-color:grey;
                margin-top:5px;
            }
            .size38:hover,
            .size39:hover,
            .size40:hover,
            .size41:hover,
            .size42:hover,
            .size43:hover,
            .size44:hover,
            .size45:hover,
            .size46:hover{
                background-color:black;
                color:white;
                cursor:pointer;
            }
           
            .catologo{
                display:flex;
                justify-content:center;
                flex-wrap: wrap;
                gap: 10px;
                
                
            }
            .sneaker-image{
                width:100%;
            }
            .sneaker{
                width:25%; 
                
                margin-top:10px;    
                text-transform:uppercase;   
                cursor:pointer; 
                transition:all 2s

             }
             .option{
                margin-left:10px;
                cursor:pointer;
             }
             
             .basket:hover,
            .tennis:hover,
            .skate:hover,
            .hiking:hover,
            .nike:hover,
            .jordan:hover,
            .lacoste:hover,
            .converse:hover,
            .vans:hover,
            .salomon:hover,
            .timberland:hover{
                
                color:#ab0e89;;
                
            }
             .sneaker:hover{
            transform:scale(1.1);
             }

             .info{
                text-align: center;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                gap: 5px;
             }
             .price{
                color:#ab0e89;
             }
        `
    ];

    static get properties() {
        return {
            sneakers: { type: Array },
            category:{type: Array},
            sizeFilter:{type: Array},
            brand:{type: Array}
        };
    }
    constructor() {
        super();
        this.sneakers = [];
        this.category = [];
        this.sizeFilter = [];
        this.brand = [];
    }

    firstUpdated(){
        fetch("https://my-json-server.typicode.com/claumartinezh/training-db/shoes")
        .then(response => response.json())
        .then(json => this.guardarDatos(json))
        .catch(error => console.error(error));
    }

    guardarDatos(json) {
        this.sneakers = [...json];
    }

    render() {
        return html`
        <div class="section">
<div class="categorias">

    <h2 class="filter"><b>Filter</b></h2>
        <h3> Category </h3>
        <div class="option">
            <option class="basket">Basketball </option>
            <option class="tennis">Tennis </option>
            <option class="skate">Skate </option>
            <option class="hiking">Hiking </option>
    </div>
        <h3> Size </h3>
        <div class="size38"> 
            <div>38</div>
    </div>
    <div class="size39"> 
            <div>39</div>
    </div>
    <div class="size40"> 
            <div>40</div>
    </div>
    <div class="size41"> 
            <div>41</div>
    </div>
    <div class="size42"> 
            <div>42</div>
    </div>
    <div class="size43"> 
            <div>43</div>
    </div>
    <div class="size44"> 
            <div>44</div>
    </div>
    <div class="size45"> 
            <div>45</div>
    </div>
    <div class="size46"> 
            <div>46</div>
    </div>

        <h3> Brand </h3>
        <div class="option">
            <option class ="nike">Nike </option>
            <option class ="jordan ">Jordan </option>
            <option class ="lacoste">Lacoste</option>
            <option class ="converse"> Converse</option>
            <option class ="vans">Vans </option>
            <option class ="salomon">Salomon </option>
            <option class ="timberland"> Timberland </option>
    </div>
</div>
<div class="catologo">
    
${this.sneakers.map(sneaker => html`
    <div class="sneaker">
                        <app-link href="/detail/${sneaker.id}">
                        <div><img class="sneaker-image" src="${sneaker.image}"/></div>
                        <div class="info">
                            <span>${sneaker.name}</span>
                            <span class="price"><b>${sneaker.price}€</b></span>
                        </div>
                        </app-link>
            </div>
            `)}

    </div> 
</div>


<div>
</div>`;
    }
    
}
customElements.define('catalogo-tienda', CatalogoTienda);
