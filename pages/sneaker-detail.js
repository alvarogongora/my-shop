import { LitElement, html, css } from 'lit-element';


export class SneakerDetail extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
            }
            .detail {
                display: flex;
                text-align: center;
                justify-content: center;
            }
            
            .lados {
                display: flex;
                gap: 10px;
                align-items: center; 
            }
            .lados img {
                max-width: 200px; 
            }
            .info {
                text-align: center;
            }
            .price{
                color:#ab0e89;
                
            }
          
            .add-to-cart{
                background-color:#333;
                color:white;
                font-family:verdana;
            }
        `
    ];
    static get properties() {
        return {
            sneaker: { type: Object },
            sneakerToCart:{type:Object},
            size_selected:{type:Object},
            idProduct: { type: Number }
        };
    }
    constructor() {
        super();
        
        this.sneaker = {};
        this.sneakerToCart={};
       
    }

    firstUpdated() {
        this.loadSneakerDetail(this.idProduct);
    }

    loadSneakerDetail(id) {
        fetch(`https://my-json-server.typicode.com/claumartinezh/training-db/shoes/${id}`)
            .then(response => response.json())
            .then(json => this.guardarDatos(json))
            .catch(error => console.error(error));
    }
    guardarDatos(json) {
        this.sneaker = json;
     
    }
    selectSize(){
        let size_selected= parseInt(this.shadowRoot.getElementById("size").value);
        this.sneakerToCart={...this.sneaker};
        console.log(size_selected)
       this.sneakerToCart = {...this.sneakerToCart, size: size_selected};

        console.log(this.sneakerToCart);
    }

    render() {
        return html`
        
        <div class="detail">
        <h1>${this.sneaker.name}</h1>
            <div>
                <img class="sneaker-image" src="${this.sneaker.image}" />
            </div>
            <div class="lados">
                <img class="sneaker-side" src="${this.sneaker['image-side']}" />
                <img class="sneaker-behind" src="${this.sneaker['image-behind']}" />
            </div>
        </div>
        <div class="info">
        
            <span class="price"><b>${this.sneaker.price}€</b></span>
            <select id="size">
            
                 ${this.sneaker.size?.map(size => html`<option .@change=${this.selectSize} >${size}</option>`)}

                </select>
                
                <button class="add-to-cart" @click="${this.addToCart}">Añadir al carrito</button>                
                
        </div>
    </div>
    `;
    }
    addToCart() { 
        this.selectSize();
        this.dispatchEvent(
            new CustomEvent("add-product-cart", {
                detail: this.sneakerToCart,
                bubbles: true,
                composed: true
            })
        )
         
        
    }

}
customElements.define('sneaker-detail', SneakerDetail);
