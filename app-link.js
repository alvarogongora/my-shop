import { LitElement, html, css } from 'lit-element';
import { navigator } from 'lit-element-router';
 

class Link extends navigator(LitElement) {
    static get properties() {
        return {
            href: { type: String }
        };
    }
    constructor() {
        super();
        this.href = '';
    }

    static get styles() {
      return css`
        :host {
          display: block;
          
        }

        a{
            color: black;
            text-decoration: none;
        }
      `;
    }

    render() {
        return html`
            <a href='${this.href}' @click='${this.linkClick}'>
                <slot></slot>
            </a>
        `;
    }
    linkClick(event) {
        event.preventDefault();
        this.navigate(this.href);
    }
}
 
customElements.define('app-link', Link);