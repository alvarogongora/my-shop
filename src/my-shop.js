import { LitElement, html, css } from 'lit-element';
import { router } from 'lit-element-router';
import '../pages/sneaker-detail.js';
import '../pages/catalogo-tienda.js';
import '../pages/mi-carrito.js';
import '../app-link.js';
import '../app-main.js';




import { Router } from '@vaadin/router';


class myShop extends router(LitElement) {
    static styles = [
        css`
            :host {
                display: block;
            }
            .header{
                height:170px;
                background-color:#2b2b2b; 
               display:flex;
               flex-direction:row;
               justify-content:space-between;
               align-items:center;
               
            }
            .imagen{
                height:50px;
                width:50px;
                border:solid;
                border-color:pink;
                border-radius:100%;
            }
            .header-text{
                color:white;
                display:flex;
            }
            .footer{
                background-color: #333; 
            color: #fff; 
            padding: 40px 0; 
            text-align: center; 
            }
            .casa{
                height:50px;
                width:50px;
                border:solid;
                border-color:pink;
                border-radius:100%;
            }
            .carrito{
                height:50px;
                width:50px;
                border:solid;
                border-color:pink;
                border-radius:100%; 
            }
        `
    ];

    static get properties() {
        return {
            sneakers: { type: Array },
            sneakerCart: { type: Array },
            route: { type: String },
            params: { type: Object },
            query: { type: Object }
        };
    }
    constructor() {
        super();
        this.sneakers = [];
        this.sneakerCart = [];
        this.route = '';

        this.params = {};

        this.query = {};
    }
    static get routes() {

        return [{

            name: 'home',

            pattern: '',

            data: { title: 'Home' }

        }, {

            name: 'detail',

            pattern: 'detail/:id'

        }, {

            name: 'cart',

            pattern: 'cart'

        }, {

            name: 'not-found',

            pattern: '*'

        }];

    }
    router(route, params, query, data) {

        this.route = route;

        this.params = params;

        this.query = query;

    }

    render() {

        return html`
        <div class="header">

           <div class="casa"> <button class="casa"><a href="/">Casa</a></button></div>
            <div class="imagen">
                <svg class="logo" xmlns="http://www.w3.org/2000/svg" height="48" width="48"><title>A 'previous sign' icon</title>
                <path d="M11 36V12h3v24Zm26 0L19.7 24 37 12Zm-3-12Zm0 6.25v-12.5L24.95 24Z"/></svg>
                <span class="header-text"><b>Men´s Lifestyle Sneakers</b></span>
            </div>
            <div class="carrito"> <button class="carrito"> <a href="/cart">Carrito</a></button></div>
        </div>

        ${this.route == "home" ? html` <catalogo-tienda route="home"></catalogo-tienda>` : ""}
        ${this.route == "detail" ? html` <sneaker-detail route="detail" idProduct=${this.params.id} @add-product-cart=${this.addProductCart}></sneaker-detail>` : ""}
        ${this.route == "cart" ? html` <mi-carrito route="cart" .cart=${this.sneakerCart} @delete-product=${this.deleteProduct}></mi-carrito >` : ""}

        <p class ="footer">FOOTER</p>
        


`;
    }

    addProductCart(e) {
        let product = e.detail;
        this.sneakerCart = [...this.sneakerCart, product];
        console.log(this.sneakerCart);
    }

    deleteProduct(e) {

        let id = e.detail;
        this.sneakerCart = this.sneakerCart.filter(sneaker => sneaker.id != id);
    }

    async connectedCallback() {
        super.connectedCallback();

    }


    firstUpdated() {
        const router = new Router(this.shadowRoot.getElementById('outlet'));
        router.setRoutes([
            { path: '/', component: 'catalogo-tienda' },
            { path: '/detail/:id', component: 'sneaker-detail' },
            { path: '/cart', component: 'mi-carrito' },
        ]);

    }

}
customElements.define('my-shop', myShop);