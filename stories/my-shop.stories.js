import { html } from 'lit';
import '../src/my-shop.js';

export default {
  title: 'MyShop',
  component: 'my-shop',
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

function Template({ header, backgroundColor }) {
  return html`
    <my-shop
      style="--my-shop-background-color: ${backgroundColor || 'white'}"
      .header=${header}
    >
    </my-shop>
  `;
}

export const App = Template.bind({});
App.args = {
  header: 'My app',
};
